/*
 * factory.cpp
 *
 *  Created on: 29-Aug-2017
 *      Author: shivujagga
 */



#include <trillBPP/factory.h>

//! \cond

namespace trill {

  template<>
  void create_elements(unsigned char* &ptr, int n, const Factory &)
  {
    void *p = operator new(sizeof(unsigned char) * n);
    ptr = reinterpret_cast<unsigned char*>(p);
  }

  template<>
  void create_elements(bin* &ptr, int n, const Factory &)
  {
    void *p = operator new(sizeof(bin) * n);
    ptr = reinterpret_cast<bin*>(p);
  }

  template<>
  void create_elements(short int* &ptr, int n, const Factory &)
  {
    void *p = operator new(sizeof(short int) * n);
    ptr = reinterpret_cast<short int*>(p);
  }

  template<>
  void create_elements(int* &ptr, int n, const Factory &)
  {
    void *p = operator new(sizeof(int) * n);
    ptr = reinterpret_cast<int*>(p);
  }

  // The following specialisation allocate memory using 16-byte alignment
  template<>
  void create_elements(double* &ptr, int n, const Factory &)
  {
    void *p0 = operator new(sizeof(double) * n + 16);
    void *p1 = reinterpret_cast<void*>((reinterpret_cast<std::size_t>(p0) + 16)
				       & (~(std::size_t(15))));
    *(reinterpret_cast<void**>(p1) - 1) = p0;
    ptr = reinterpret_cast<double*>(p1);
  }

  // The following specialisation allocate memory using 16-byte alignment
  template<>
  void create_elements(std::complex<double>* &ptr, int n, const Factory &)
  {
    void *p0 = operator new(sizeof(std::complex<double>) * n + 16);
    void *p1 = reinterpret_cast<void*>((reinterpret_cast<std::size_t>(p0) + 16)
				       & (~(std::size_t(15))));
    *(reinterpret_cast<void**>(p1) - 1) = p0;
    ptr = reinterpret_cast<std::complex<double>*>(p1);
  }


  template<>
  void destroy_elements(unsigned char* &ptr, int n)
  {
    if (ptr) {
      void *p = reinterpret_cast<void*>(ptr);
      operator delete(p);
    }
  }

  template<>
  void destroy_elements(bin* &ptr, int n)
  {
    if (ptr) {
      void *p = reinterpret_cast<void*>(ptr);
      operator delete(p);
      ptr = 0;
    }
  }

  template<>
  void destroy_elements(short int* &ptr, int n)
  {
    if (ptr) {
      void *p = reinterpret_cast<void*>(ptr);
      operator delete(p);
      ptr = 0;
    }
  }

  template<>
  void destroy_elements(int* &ptr, int n)
  {
    if (ptr) {
      void *p = reinterpret_cast<void*>(ptr);
      operator delete(p);
      ptr = 0;
    }
  }


  template<>
  void destroy_elements(double* &ptr, int n)
  {
    if (ptr) {
      void *p = *(reinterpret_cast<void**>(ptr) - 1);
      operator delete(p);
      ptr = 0;
    }
  }

  template<>
  void destroy_elements(std::complex<double>* &ptr, int n)
  {
    if (ptr) {
      void *p = *(reinterpret_cast<void**>(ptr) - 1);
      operator delete(p);
      ptr = 0;
    }
  }

}

//! \endcond

