/*
 * misc.cpp
 *
 *  Created on: 29-Aug-2017
 *      Author: shivujagga
 */


#include <trillBPP/misc.h>


namespace trill
{

std::string itpp_version(void)
{
#ifdef PACKAGE_VERSION
  return std::string(PACKAGE_VERSION);
#else
  return std::string("Warning: Version unknown!");
#endif
}

bool is_bigendian()
{
  int i = 1;
  char *p = reinterpret_cast<char *>(&i);
  if (p[0] == 1) // Lowest address contains the least significant byte
    return false; // LITTLE_ENDIAN
  else
    return true; // BIG_ENDIAN
}


///Conjugate from it/math/elem_math


} //namespace itpp
