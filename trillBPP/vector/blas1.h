/*
 * blas1.h
 *
 *  Created on: 30-Aug-2017
 *      Author: shivujagga
 */

#ifndef TRILLBPP_BLAS1_H_
#define TRILLBPP_BLAS1_H_

#include <complex>

//! \cond

#if defined(_MSC_VER) && defined(HAVE_ACML)
#  define dswap_ DSWAP
#  define zswap_ ZSWAP
#  define dscal_ DSCAL
#  define zscal_ ZSCAL
#  define dcopy_ DCOPY
#  define zcopy_ ZCOPY
#  define daxpy_ DAXPY
#  define zaxpy_ ZAXPY
#  define ddot_  DDOT
#  define dgemv_ DGEMV
#  define zgemv_ ZGEMV
#  define dger_  DGER
#  define zgeru_ ZGERU
#  define zgerc_ ZGERC
#  define dgemm_ DGEMM
#  define zgemm_ ZGEMM
#elif defined(_MSC_VER) && defined(HAVE_MKL)
#  define dswap_ mkl_blas_dswap
#  define zswap_ mkl_blas_zswap
#  define dscal_ mkl_blas_dscal
#  define zscal_ mkl_blas_zscal
#  define dcopy_ mkl_blas_dcopy
#  define zcopy_ mkl_blas_zcopy
#  define daxpy_ mkl_blas_daxpy
#  define zaxpy_ mkl_blas_zaxpy
#  define ddot_  mkl_blas_ddot
#  define dgemv_ mkl_blas_dgemv
#  define zgemv_ mkl_blas_zgemv
#  define dger_  mkl_blas_dger
#  define zgeru_ mkl_blas_zgeru
#  define zgerc_ mkl_blas_zgerc
#  define dgemm_ mkl_blas_dgemm
#  define zgemm_ mkl_blas_zgemm
#endif // #if defined(_MSC_VER) && (defined(HAVE_ACML) || defined(HAVE_MKL))


namespace blas
{

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

  // ----------------------------------------------------------------------
  // BLAS 1 functions
  // ----------------------------------------------------------------------

  void dswap_(const int *n,
              double *x, const int *incx,
              double *y, const int *incy);

  void zswap_(const int *n,
              std::complex<double> *x, const int *incx,
              std::complex<double> *y, const int *incy);

  void dscal_(const int *n,
              const double *alpha,
              double *x, const int *incx);

  void zscal_(const int *n,
              const std::complex<double> *alpha,
              std::complex<double> *x, const int *incx);

  void dcopy_(const int *n,
              const double *x, const int *incx,
              double *y, const int *incy);

  void zcopy_(const int *n,
              const std::complex<double> *x, const int *incx,
              std::complex<double> *y, const int *incy);

  void daxpy_(const int *n,
              const double *alpha,
              const double *x, const int *incx,
              double *y, const int *incy);

  void zaxpy_(const int *n,
              const std::complex<double> *alpha,
              const std::complex<double> *x, const int *incx,
              std::complex<double> *y, const int *incy);

  double ddot_(const int *n,
               const double *x, const int *incx,
               const double *y, const int *incy);

  // ----------------------------------------------------------------------
  // BLAS 2 functions
  // ----------------------------------------------------------------------

  void dgemv_(const char *transA, const int *m, const int *n,
              const double *alpha,
              const double *A, const int *ldA,
              const double *x, const int *incx,
              const double *beta,
              double *y, const int *incy);

  void zgemv_(const char *transA, const int *m, const int *n,
              const std::complex<double> *alpha,
              const std::complex<double> *A, const int *ldA,
              const std::complex<double> *x, const int *incx,
              const std::complex<double> *beta,
              std::complex<double> *y, const int *incy);

  void dger_(const int *m, const int *n,
             const double *alpha,
             const double *x, const int *incx,
             const double *y, const int *incy,
             double *A, const int *ldA);

  void zgeru_(const int *m, const int *n,
              const std::complex<double> *alpha,
              const std::complex<double> *x, const int *inxx,
              const std::complex<double> *y, const int *incy,
              std::complex<double> *A, const int *ldA);

  void zgerc_(const int *m, const int *n,
              const std::complex<double> *alpha,
              const std::complex<double> *x, const int *inxx,
              const std::complex<double> *y, const int *incy,
              std::complex<double> *A, const int *ldA);

  // ----------------------------------------------------------------------
  // BLAS 3 functions
  // ----------------------------------------------------------------------

  void dgemm_(const char *transA, const char *transB,
              const int *m, const int *n, const int *k,
              const double *alpha,
              const double *A, const int *ldA,
              const double *B, const int *ldB,
              const double *beta,
              double *C, const int *ldC);

  void zgemm_(const char *transA, const char *transB,
              const int *m, const int *n, const int *k,
              const std::complex<double> *alpha,
              const std::complex<double> *A, const int *ldA,
              const std::complex<double> *B, const int *ldB,
              const std::complex<double> *beta,
              std::complex<double> *C, const int *ldC);

#ifdef __cplusplus
}
#endif /* __cplusplus */

} // namespace blas

//! \endcond



#endif /* BLAS1_H_ */
