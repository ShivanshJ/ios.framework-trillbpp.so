/*
 * trigger.cc
 *
 *  Created on: May 27, 2017
 *      Author: Shivansh
 */
#include <trillBPP/trigger.h>

#define Fs 44100
#define G .55
#define H 1.1


int getIndexForFreq(int freq, int len, int fs) {
    int index = ( ( len / (float) fs ) * freq);
    return index;
}

vec getFilterdAmplitude(vec amp, int size) {
    int sizeAmp = amp.size();
    int freqIndex14k = getIndexForFreq(14000, size, Fs);
    vec zeroArray = amp.get(0, freqIndex14k-1);
    zeroArray.zeros();
    return concat(zeroArray, amp.get(freqIndex14k, sizeAmp-1));
}

double getAverage(vec Vec, int start, int end) {
    double avg;
    double size = end - start;
    for (int i = start; i<end; i++) {
        avg += Vec[i];
    }
    return avg / size;
}

vec createChunks(vec amp, int chunk_length) {
    
    vec chunkedArray;
    int counter = 0;
    for (int i=0; i<amp.size(); i = i + chunk_length){
        double chunkSelect = getAverage(amp, i, (i+chunk_length));
        chunkedArray.ins(counter, chunkSelect);
        counter = counter + 1;
    }
    
    return chunkedArray;
}

vec getDiversionSquare(vec amp, double mean, int Index14K) {
    vec diversionSquare;
    int counter = 0;
    
    for (int i=Index14K; i<amp.size(); i++) {
        double dataSelected = amp[i]-mean;
        dataSelected = dataSelected * dataSelected;
        diversionSquare.ins(counter, dataSelected);
        counter = counter + 1;
    }
    
    return diversionSquare;
}

std::pair<double, double> getGammaValue(vec diversionValuesSquare, double mean) {
    
    double sigmaSq = getAverage(
                                diversionValuesSquare,
                                0,
                                diversionValuesSquare.size()
                                );
    
    double sigma = pow(sigmaSq, 0.5);
    double gamma = ( (G * sigmaSq) + (H * sigma) ) / mean;
    
    return make_pair(gamma, sigmaSq);
    
}

std::pair<double, double> checkCondition(vec amp, double gamma, double mean){
    int sizeAmp = amp.size();
    
    int freq14KIndex = getIndexForFreq(14000, sizeAmp, 22050) + 1;
    int freq16KIndex = getIndexForFreq(16000, sizeAmp, 22050) + 1;
    int freq18KIndex = getIndexForFreq(18000, sizeAmp, 22050) + 1;
    
    double gamma14K = 0.1;
    int counter14 = 0;
    double gamma16K = 0.1;
    int counter16 = 0;
    
    for( int i =freq14KIndex; i < freq18KIndex; i++) {
        if(i < freq16KIndex ) {
            if (abs(mean - amp[i]) > gamma) {
                gamma14K += amp[i];
                counter14 += 1;
                
            }
        }
        else {
            if (abs(mean - amp[i]) > gamma) {
                gamma16K += amp[i];
                counter16 += 1;
            }
        }
    }
    
    if (gamma14K > 0.1) {
        gamma14K = gamma14K / counter14;
    }
    if (gamma16K > 0.1) {
        gamma16K = gamma16K / counter16;
    }
    
    double ratio = gamma16K / gamma14K;
    return make_pair(gamma14K, gamma16K);
}


float sumVector(vec data, int starting_point, int ending_point) {
    float result = 0;
    for (int i=starting_point; i < ending_point; i++) {
        result += (data[i]*data[i]);
    }
    return result;
}

void Trigger::isFound(cvec input, float *result_array) {
    int freq_index_17k = getIndexForFreq(17000, input.size(), 44100);
    int freq_index_19k = getIndexForFreq(19500, input.size(), 44100);
    
    cvec out = fft(input);
    vec mag = abs(out);
    
    float sum_17k_19k = sumVector(mag, freq_index_17k, freq_index_19k);
    
    for (int i = 0; i < 5; i++) {
        int selectedFreq_1 = 17000 + (i* 500);
        int selectedFreq_2 = 17000 + ((i+1)*500);
        int selectedFreqIndex_1 = getIndexForFreq(selectedFreq_1, input.size(), 44100);
        int selectedFreqIndex_2 = getIndexForFreq(selectedFreq_2, input.size(), 44100);
        float selectedBandData = ( sumVector( mag, selectedFreqIndex_1, selectedFreqIndex_2 ) / sum_17k_19k ) * 100;
        result_array[i] = selectedBandData;
    }
}

void Trigger::windowCall(cvec input, int &chunkSize, int &windowSize, int *numbers, int *power_percentage){
    int num_items = input.size();
    int ratio = chunkSize/windowSize;
    cvec divided_storage[ratio];
    
    for(int i=0; i < ratio; i++ )
        divided_storage[i] = input.get(i*windowSize, (i+1)*windowSize - 1);
    
    float power_array[5];
    for(int i=0; i<ratio; i++){
        isFound(divided_storage[i], power_array);
        numbers[i] = -1;
        power_percentage[i] = 0;
        for(int j=0; j<5; j++){
            if(power_array[j]>60){
                power_percentage[i] = power_array[j];
                numbers[i] = j+1;
                break;
            }
        }
    }
}

