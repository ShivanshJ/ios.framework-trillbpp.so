/*
 * modulation.cc
 *
 *  Created on: 29-Aug-2017
 *      Author: shivujagga
 */


#include <trillBPP/modulation.h>



//###########################################Convolution
vec modulation::convolve_time_domain(vec data_bits, vec rrc_wave) {
	int lenA = data_bits.size();
	int lenB = rrc_wave.size();
	int lenC = lenA + lenB - 1;
	vec c(lenC);
	int k;
	double tmp;
	for (int i = 0; i < lenC; i++) {
		k = i;
		tmp = 0.0;
		for (int j = 0; j < lenB; j++) {
			if(k >= 0 && k < lenA)
				tmp = tmp + (data_bits[k]*rrc_wave[j]);
			k = k-1;
			c[i] = tmp;
		}
	}

	int maxLen;
	if(lenA > lenB) {
		maxLen = lenA;
	} else {
		maxLen = lenB;
	}
	int diff = lenC - maxLen;
	int i = 0;
	while(i < diff) {
		c.del(c.size()-1);
		i++;
		if(i < diff) {
			c.del(0);
			i++;
		}
	}
	return c;
}

vec modulation::convolve_freq_domain(vec data_bits, vec rrc_wave){
	int lenA = data_bits.size();
	int lenB = rrc_wave.size();
	int lenC = lenA + lenB -1;
	vec c(lenC);
//Complex data-type for inputting our data_bits and rrc_wave
	fftw_complex in_data[lenC];
	fftw_complex in_rrc[lenC];
//@flag - to identify which  is maxlength, 0 for len a : 1 for lenb
	int flag;
	if(lenA>lenB)
		flag = 0;
	else flag = 1;
//Zero-padding the data from their size till lenC
	int i;
	if(flag == 0){
		for(i = 0; i < lenA; i++ ){
			in_data[i][0] = data_bits[i];
			in_data[i][1] = 0;
			if( i<lenB ){
				in_rrc[i][0] = rrc_wave[i];
				in_rrc[i][1] = 0;
			} else //else start zero-padding @rrc_wave
			{ in_rrc[i][0] = 0;
			  in_rrc[i][1] = 0;
			}
		}
		for(i=lenA ; i < lenC; i++ ){	//ZeroPad till @lenC
			in_data[i][0] = 0;
			in_data[i][1] = 0;
			in_rrc[i][0] = 0;
			in_rrc[i][1] = 0;
		}
	}
	else{
		for(i = 0; i < lenB; i++ ){
			in_rrc[i][0] = rrc_wave[i];
			in_rrc[i][1] = 0;
			if( i<lenA ){
				in_data[i][0] = data_bits[i];
				in_data[i][1] = 0;
			} else ////else start zero-padding @data_bits
			{ in_data[i][0] = 0;
			  in_data[i][1] = 0;
			}
		}
		for(i=lenB ; i < lenC; i++ ){	//ZeroPad till @lenC
			in_data[i][0] = 0;
			in_data[i][1] = 0;
			in_rrc[i][0] = 0;
			in_rrc[i][1] = 0;
		}
	}
//FFTW functions and variable introduced to store frequency
	fftw_complex freq_data[lenC], freq_rrc[lenC];
	fftw_complex rev_data[lenC], time_data[lenC];
	fftw_plan p1 = fftw_plan_dft_1d(lenC, in_data, freq_data, FFTW_FORWARD ,FFTW_ESTIMATE);
	fftw_plan p2 = fftw_plan_dft_1d(lenC, in_rrc, freq_rrc, FFTW_FORWARD, FFTW_ESTIMATE);
	fftw_plan rev = fftw_plan_dft_1d(lenC, rev_data, time_data,FFTW_BACKWARD, FFTW_ESTIMATE);
	fftw_execute(p1);
	fftw_execute(p2);
//    double SCALE = lenC;
    for( i = 0; i < lenC ; i++ ) {
        double realD = freq_data[i][0];
        double imagD = freq_data[i][1];
        double realS = freq_rrc[i][0];
        double imagS = freq_rrc[i][1];
        rev_data[i][0] = (realD * realS - imagD * imagS);//*SCALE;
        rev_data[i][1] = (realD * imagS + imagD * realS);//*SCALE;
    }
    fftw_execute(rev);

	for( i = 0; i < lenC ; i++ ){
//In Inverse FFT in  C++ fftw, each data-point is scaled by the length of the array, here by @lenC
        c[i] = time_data[i][0]/lenC;
    }
//The following block deletes array portion from end and beginning to get the middle parts
//	int maxLen;
//	if(lenA > lenB) {
//		maxLen = lenA;
//	} else {
//		maxLen = lenB;
//	}
//	int diff = lenC - maxLen;
//	i = 0;
//	while(i < diff) {
//		c.del(c.size()-1);
//		i++;
//		if(i < diff) {
//			c.del(0);
//			i++;
//		}
//	}
    fftw_destroy_plan(p1); fftw_destroy_plan(p2); fftw_destroy_plan(rev);
	return c;
}

//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//###########################################Correlation
vec modulation::xcorr(const vec &x, const int max_lag, const std::string scaleopt)
{
  cvec out(2*x.length() - 1); //Initial size does ont matter, it will get adjusted
  xcorr(to_cvec(x), to_cvec(x), out, max_lag, scaleopt, true);

  return real(out);
}
vec modulation::xcorr(const vec &x, const vec &y, const int max_lag, const std::string scaleopt)
{
  cvec out(2*x.length() - 1); //Initial size does ont matter, it will get adjusted
  xcorr(to_cvec(x), to_cvec(y), out, max_lag, scaleopt, false);

  return real(out);
}


void modulation::xcorr(const cvec &x, const cvec &y, cvec &out, const int max_lag, const std::string scaleopt, bool autoflag){

	int N = std::max(x.length(), y.length());

	  //Compute the FFT size as the "next power of 2" of the input vector's length (max)
	int b = static_cast<int>(std::ceil( log(N)/log(2)) );
	int fftsize = ((b < 0) ? 0 : (1 << b));
	int end=fftsize-1;

	  cvec temp2;
	  if (autoflag == true) {
	    //Take FFT of input vector
	    cvec X = fft(x, fftsize);

	    //Compute the abs(X).^2 and take the inverse FFT.
	    temp2 = ifft(elem_mult(X, conj(X) ));
	  }
	  else {
	    //Take FFT of input vectors
	    cvec X = fft(zero_pad(x, fftsize));
	    cvec Y = fft(zero_pad(y, fftsize));

	    //Compute the crosscorrelation
	    temp2 = ifft(elem_mult(X, conj(Y)));
	  }

	  // Compute the total number of lags to keep. We truncate the maximum number of lags to N-1.
	  int maxlag;
	  if ((max_lag == -1) || (max_lag >= N))
	    maxlag = N - 1;
	  else
	    maxlag = max_lag;


	  //Move negative lags to the beginning of the vector. Drop extra values from the FFT/IFFt
	  if (maxlag == 0) {
	    out.set_size(1, false);
	    out = temp2(0);
	  }
	  else //out = temp2;
	    out = concat(temp2(end - maxlag + 1, end), temp2(0, maxlag));

	  //Scale data
//	    if (scaleopt == "biased")
//	      //out = out / static_cast<double_complex>(N);
//	      out = out / static_cast<std::complex<double> >(N);
//	    else if (scaleopt == "unbiased") {
//	      //Total lag vector
//	      vec lags = linspace(-maxlag, maxlag, 2 * maxlag + 1);
//	      cvec scale = to_cvec(static_cast<double>(N) - abs(lags));
//	      out /= scale;
//	    }
//	    else if (scaleopt == "coeff") {
//	      if (autoflag == true) // Normalize by Rxx(0)
//	        out /= out(maxlag);
//	      else { //Normalize by sqrt(Rxx(0)*Ryy(0))
//	        double rxx0 = sum(abs(elem_mult(x, x)));
//	        double ryy0 = sum(abs(elem_mult(y, y)));
//	        out /= std::sqrt(rxx0 * ryy0);
//	      }
//	    }
//	    else if (scaleopt == "none") {}
//	    else
//	      it_warning("Unknow scaling option in XCORR, defaulting to <none> ");
}


//void modulation::xcorr(const cvec &x, const cvec &y, cvec &out, const int max_lag, const std::string scaleopt, bool autoflag)
//{
//  int N = std::max(x.length(), y.length());
//  //Compute the FFT size as the "next power of 2" of the input vector's length (max)
//  int b = static_cast<int>(std::ceil( log(N)/log(2)) );
//  int fftsize = ((b < 0) ? 0 : (1 << b));
//  int end=fftsize-1;
//	 //Write cvec into fftw_complex data type
//	 fftw_complex fftw_x[fftsize];
//	 fftw_complex fftw_y[fftsize];
//	 //FFTW functions and variable introduced to store frequency
//	 fftw_complex freq_x[fftsize], freq_y[fftsize];
//	 fftw_complex rev_data[fftsize], time_data[fftsize];
//	 fftw_plan fft_x = fftw_plan_dft_1d(fftsize, fftw_x, freq_x, FFTW_FORWARD ,FFTW_ESTIMATE);
//	 fftw_plan fft_y = fftw_plan_dft_1d(fftsize, fftw_y, freq_y, FFTW_FORWARD, FFTW_ESTIMATE);
//	 fftw_plan ifft = fftw_plan_dft_1d(fftsize, rev_data, time_data,FFTW_BACKWARD, FFTW_ESTIMATE);
//
//  cvec var_for_return(fftsize);
//  if (autoflag == true) {
//	  //NOTE : It means both, x and y are the same vectors, hence x.length()==y.length()
//	  for (int i = 0; i < x.length(); ++i) {
//		  fftw_x[i][0] = x[i].real();
//		  fftw_x[i][1] = x[i].imag();
//	  }
//	  //Zero-pad to the  "next power of 2"
//	  for (int i = x.length(); i < fftsize; ++i) {
//		  fftw_x[i][0] = 0;
//		  fftw_x[i][1] = 0;
//	  }
//	  fftw_execute(fft_x);
//	  //NOTE : We want the conjugate of fftw_y for CORELATION
//	  for(int i = 0; i < fftsize ; i++ ) {
//		  double realX = freq_x[i][0];
//		  double imagX = freq_x[i][1];
//		  double realY = freq_x[i][0];
//		  double imagY = (-1)*freq_x[i][1];	//We want the conjugate of fftw_y for CORELATION
//		  rev_data[i][0] = (realX * realY - imagX * imagY);//*SCALE;
//		  rev_data[i][1] = (realX * imagY + imagX * realY);//*SCALE;
//	  }
//	  fftw_execute(ifft);
//	  //Convert to cvec
//	  for (int i = 0; i < fftsize; ++i) {
//		  var_for_return(i) = std::complex<double>( time_data[i][0], time_data[i][1] );
//	  }
//  }
////ELSE
//  else {
//	  for (int i = 0; i < x.length(); ++i) {
//		  fftw_x[i][0] = x[i].real();
//		  fftw_x[i][1] = x[i].imag();
//	  }
//	  for (int i = 0; i < y.length(); ++i) {
//		  fftw_y[i][0] = y[i].real();
//		  fftw_y[i][1] = y[i].imag();
//	  }
//	  //Zero-pad to the  "next power of 2"
//	  for (int i = x.length(); i < fftsize; ++i) {
//		  fftw_x[i][0] = 0;
//		  fftw_x[i][1] = 0;
//	  }
//	  for (int i = y.length(); i < fftsize; ++i) {
//		  fftw_y[i][0] = 0;
//		  fftw_y[i][1] = 0;
//	  }
//	  fftw_execute(fft_x);
//	  fftw_execute(fft_y);
//	  //NOTE : We want the conjugate of fftw_y for CORELATION
//	  for(int i = 0; i < fftsize ; i++ ) {
//		  double realX = freq_x[i][0];
//		  double imagX = freq_x[i][1];
//		  double realY = freq_y[i][0];
//		  double imagY = (-1)*freq_y[i][1];	//We want the conjugate of fftw_y for CORELATION
//		  rev_data[i][0] = (realX * realY - imagX * imagY);//*SCALE;
//		  rev_data[i][1] = (realX * imagY + imagX * realY);//*SCALE;
//	  }
//	  fftw_execute(ifft);
//	  //Convert to cvec
//	  for (int i = 0; i < fftsize; ++i) {
//		  var_for_return(i) = std::complex<double>( time_data[i][0]/fftsize, time_data[i][1]/fftsize );
//	  }
//  }
//  // Compute the total number of lags to keep. We truncate the maximum number of lags to N-1.
//  int maxlag;
//  if ((max_lag == -1) || (max_lag >= N))
//    maxlag = N - 1;
//  else
//    maxlag = max_lag;
//
//  //Move negative lags to the beginning of the vector. Drop extra values from the FFT/IFFt
//  if (maxlag == 0) {
//    out.set_size(1, false);
//    out = var_for_return(0);
//  }
//  else{
//	  //Originally in ITPP
//	  out = concat(var_for_return(end - maxlag + 1, end), var_for_return(0, maxlag));
////	  out = var_for_return(0, N - std::min(x.length(), y.length()) + 1);
//  }
//  //Scale data
//  if (scaleopt == "biased")
//    out = out / static_cast<std::complex<double> >(N);
////  else if (scaleopt == "unbiased") {
////    //Total lag vector
////    vec lags = linspace(-maxlag, maxlag, 2 * maxlag + 1);
////    cvec scale = to_cvec(static_cast<double>(N) - abs(lags));
////    out /= scale;
////  }
////  else if (scaleopt == "coeff") {
////    if (autoflag == true) // Normalize by Rxx(0)
////      out /= out(maxlag);
////    else { //Normalize by sqrt(Rxx(0)*Ryy(0))
////      double rxx0 = sum(abs(elem_mult(x, x)));
////      double ryy0 = sum(abs(elem_mult(y, y)));
////      out /= std::sqrt(rxx0 * ryy0);
////    }
////  }
//  else if (scaleopt == "none") {}
//  else
//    it_warning("Unknown scaling option in XCORR, defaulting to <none> ");
//
//}

//cvec modulation::fft(cvec input){
//	int size = input.size();
//	fftw_complex in_data[size], freq_data[size];
//	fftw_plan fft_input = fftw_plan_dft_1d(size, in_data, freq_data, FFTW_FORWARD ,FFTW_ESTIMATE);
//
//	for(int i = 0; i < size; i++ ){
//		in_data[i][0] = input[i].real();
//		in_data[i][1] = input[i].imag();
//	}
//	fftw_execute(fft_input);
//	cvec output(size);
//	for (int i = 0; i < size; ++i) {
//		output(i) = std::complex<double>( freq_data[i][0], freq_data[i][1]);
//	}
//	return output;
//}

