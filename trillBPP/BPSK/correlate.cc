/*
 * correlate.cc

 *
 *  Created on: Dec 21, 2016
 *      Author: rajanya
 */
#include <trillBPP/correlate.h>
#include <trillBPP/modulation.h>

using namespace trill;

vec Correlate::apply(vec wave1, vec wave2) {
	int size1 = wave1.size();
	int size2 = wave2.size();

	modulation m;
	vec resCorr = m.xcorr( wave1, wave2, -1, "none" );

	int size = resCorr.size();

	//We need an ouput matching python, which as corelation upto N-M+1
		//What we get is a size which has [ end -(N-1)+1 to end , 0 to (N-1)] concatenated.
		 int N = std::max(size1, size2);
		 int min = std::min(size1, size2);
		  //Compute the FFT size as the "next power of 2" of the input vector's length (max)
		 int b = static_cast<int>(std::ceil( log(N)/log(2)) );
		 int fftsize = ((b < 0) ? 0 : (1 << b));
		 int end=fftsize-1;

		int noOfElemsToRemoveFromBack = min -1;
		resCorr.del(size-noOfElemsToRemoveFromBack, size-1);
		int noOfElemsToRemoveFromFront = end - ( end - (N-1)  );
		resCorr.del(0,noOfElemsToRemoveFromFront-1);


		//	//We need an ouput matching python, which as corelation upto N-M+1
		//	//What we get is a size which has [ end -(N-1)+1 to end , 0 to (N-1)] concatenated.
		//	int N = std::max(size1, size2);
		//	int min = std::min(size1, size2);
		//	//Compute the FFT size as the "next power of 2" of the input vector's length (max)
		//	int b = static_cast<int>(std::ceil( log(N)/log(2)) );
		//	int fftsize = ((b < 0) ? 0 : (1 << b));
		//	int end=fftsize-1;
		//
		//	printf("\n\ncorrelate.cc size resCOrr %d fftsize %d",size, end);
		//	//The size we want is N-M+1 from the middle, i.e, the valid np.correlate mdoe in python
		//	int final_corr_size = N - min + 1;
		//	printf("\nfinal_corr_size %d",final_corr_size);
		//	int noOfElemsToRemove = (size - final_corr_size)/2;
		//	printf("no of elements from front %d",noOfElemsToRemove);
		//	resCorr.del(0,noOfElemsToRemove-1);
		//	size = resCorr.size();
		//	resCorr.del(size-noOfElemsToRemove, size-1);	//From back

	return resCorr;
}
