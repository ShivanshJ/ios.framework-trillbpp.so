/*
 * miscfunc.cc
 *
 *  Created on: 29-Sep-2017
 *      Author: shivujagga
 */


#include <trillBPP/miscfunc.h>

namespace trill {

	cvec conj(const cvec &x) {
	  cvec temp(x.size());

	  for (int i = 0; i < x.size(); i++) {
		temp(i) = std::conj(x(i));
	  }
	  return temp;
	}

	cmat conj(const cmat &x) {
	  cmat temp(x.rows(), x.cols());

	  for (int i = 0; i < x.rows(); i++) {
		for (int j = 0; j < x.cols(); j++) {
		  temp(i, j) = std::conj(x(i, j));
		}
	  }

	  return temp;
	}

	//Getting real part of cvec as vec
	vec real(const cvec &data) {
	  vec temp(data.length());

	  for (int i = 0;i < data.length();i++)
	    temp[i] = data[i].real();

	  return temp;
	}

	vec abs(const cvec &data) {
	  vec temp(data.length());

	  for (int i = 0;i < data.length();i++)
	    temp[i] = std::abs(data[i]);

	  return temp;
	}

	// ----------------------------------------------------------------------
	// Instantiations
	// ----------------------------------------------------------------------
	template cvec to_cvec(const bvec &v);
	template cvec to_cvec(const svec &v);
	template cvec to_cvec(const ivec &v);
	template cvec to_cvec(const vec &v);

	extern template vec zero_pad(const vec &v, int n);
	extern template cvec zero_pad(const cvec &v, int n);
	extern template ivec zero_pad(const ivec &v, int n);
	extern template svec zero_pad(const svec &v, int n);
	extern template bvec zero_pad(const bvec &v, int n);


} //trill

